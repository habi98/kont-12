const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const config = require('./config');

const users = require('./app/users');
const pictures = require('./app/pictures');

const app = express();

app.use(express.json());
app.use(express.static('public'));
app.use(cors());

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {

    app.use('/users', users);
    app.use('/pictures', pictures);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});
