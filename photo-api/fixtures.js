const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');
const User = require('./models/User');
const Picture = require('./models/Picture');


const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }


  const user = await User.create(
        {
            username: 'Ben Ascren',
            password: '123',
            token: nanoid()
        },
        {
            username: 'John Doe',
            password: '123',
            token: nanoid()
        }
    );

    await Picture.create(
        {
            user: user[0]._id,
            title: 'The chainsmokers',
            image: 'the-chainsmokers.jpg'
        },
        {
            user: user[1]._id,
            title: 'Maroon 5',
            image: 'maroon.jpg'
        }
    );

    await connection.close();
};

run().catch(error => {
    console.error('Something went wrong', error);
});
