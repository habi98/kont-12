const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const config = require('../config');

const auth = require('../middleware/auth');
const Picture = require('../models/Picture');



const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const router = express.Router();


router.get('/', (req, res) => {

    const criteria = {};

    if  (req.query.user) {
        criteria.user = req.query.user
    }

    Picture.find(criteria).populate({path: 'user', select: 'username' ,})
        .then(categories => res.send(categories))
        .catch(() => res.sendStatus(500));
});



router.post('/', auth, upload.single('image'), async (req, res) => {

    const productData = req.body;


    if (req.file) {
        productData.image = req.file.filename;
    }

    const picture = new Picture(productData);

    picture.user = req.user._id;

    picture.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});


router.delete('/:id', auth, async (req, res) => {
    try {
        const picture = await Picture.findByIdAndDelete({_id: req.params.id});

        if (!picture) {
            return res.sendStatus(404)
        }

        await picture.save();
        res.status(200).send('deleted successfully')

    } catch (e) {
        res.sendStatus(500)
    }
});

module.exports = router;
