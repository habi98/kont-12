import React, {Component, Fragment} from 'react';
import './App.css';
import Toolbar from "./components/UI/Toolbar";
import Container from "reactstrap/es/Container";
import {Route, Switch} from "react-router-dom";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import {NotificationContainer} from "react-notifications";
import {connect} from "react-redux";
import Pictures from "./containers/Pictures/Pictures";
import AddPictureForm from "./containers/AddPictureForm/AddPictureForm";
import UserPictures from "./containers/UserPictures/UserPictures";

class App extends Component {
    render() {
        return (
         <Fragment>
             <Toolbar user={this.props.user}/>
             <NotificationContainer/>
             <Container>
                 <Switch>
                     <Route path="/" exact component={Pictures}/>
                     <Route path="/register" exact component={Register}/>
                     <Route path="/login" exact component={Login}/>
                     <Route path="/add_picture" exact component={AddPictureForm}/>
                     <Route path="/pictures/:id" exact component={UserPictures}/>
                 </Switch>
             </Container>
         </Fragment>
        );
    }

}


const mapStateToProps = state => ({
  user: state.reducer.user
});

export default connect(mapStateToProps)(App);
