import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Card, CardBody, CardColumns, CardImg, CardTitle} from "reactstrap";
import {deletePicture, fetchPictures} from "../../store/actions";
import Modal from "../../components/UI/Modal/Modal";

class UserPictures extends Component {
    state = {
        modal: false,
        picture: null
    };

    componentDidMount() {
        this.props.fetchPictures(this.props.match.params.id)
    }

    add_Picture = () => {
       this.props.history.push('/add_picture')
    };


    show = (picture) => {
        this.setState({
            modal: true,
            picture: picture
        })
    };

    close = () => {
        this.setState({
            modal: false,
            picture: null
        })
    };

    render() {
        return (
            <Fragment>
                <h3 className="pt-3 pb-3">
                    {this.props.user &&  (
                            this.props.user._id === this.props.match.params.id ? <Button onClick={this.add_Picture} className="float-right">Add new photo</Button>: null
                    )}
                </h3>
                <CardColumns>

                {this.props.pictures.map(picture => (
                    <Card key={picture._id} >
                        <CardImg onClick={() => this.show(picture)} top width="100%" src={'http://localhost:8000/uploads/' + picture.image} alt="Card image cap" />
                        <CardBody>
                            <h3>By: {picture.user.username}</h3>
                            <CardTitle>{picture.title}</CardTitle>

                                {this.props.user && this.props.user._id === picture.user._id ? <Button onClick={() => this.props.deletePicture(picture._id, picture.user._id)} color="danger">Удалить</Button>: null}

                        </CardBody>
                    </Card>
                ))}

                </CardColumns>
                <Modal isOpen={this.state.modal} close={this.close}  show={this.show}>
                    {this.state.picture && <CardImg top width="100%" src={'http://localhost:8000/uploads/' +  this.state.picture.image} alt="Card image cap" />}
                </Modal>

            </Fragment>
        );
    }
}


const mapStateToProps = state => ({
    pictures: state.reducer.pictures,
    user: state.reducer.user
});

const mapDispatchToProps = dispatch => ({
    fetchPictures: userID => dispatch(fetchPictures(userID)),
    deletePicture: (pictureId, userId) => dispatch(deletePicture(pictureId, userId))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserPictures);