import React, {Component, Fragment} from 'react';
import {Card, CardBody, CardColumns, CardImg, CardTitle,} from "reactstrap";
import {connect} from "react-redux";
import {fetchPictureId, fetchPictures} from "../../store/actions";
import {Link} from "react-router-dom";
import Modal from "../../components/UI/Modal/Modal";

class Pictures extends Component {
    state = {
        modal: false,
        picture: null
    };

    show = (picture) => {
        this.setState({
            modal: true,
            picture: picture
        })
    };

    close = () => {
      this.setState({
          modal: false
      })
    };

    componentDidMount() {
        this.props.fetchPictures()
    }



    render() {
        return (
            <Fragment>
                <CardColumns>
                    {this.props.pictures.map(picture => (
                        <Card key={picture._id}  >
                            <CardImg onClick={() => this.show(picture)} top width="100%" src={'http://localhost:8000/uploads/' + picture.image} alt="Card image cap" />
                            <CardBody>
                                <Link to={'/pictures/' + picture.user._id}>
                                    <h3>By: {picture.user.username}</h3>
                                    <CardTitle>{picture.title}</CardTitle>
                                </Link>
                            </CardBody>
                        </Card>
                    ))}
                </CardColumns>
                <Modal isOpen={this.state.modal} close={this.close}  show={this.show}>
                    {this.state.picture && <CardImg top width="100%" src={'http://localhost:8000/uploads/' +  this.state.picture.image} alt="Card image cap" />}
                </Modal>

            </Fragment>

        );
    }
}

const mapStateToProps = state => ({
    pictures: state.reducer.pictures,
});

const mapDispatchToProps = dispatch => ({
    fetchPictures: () => dispatch(fetchPictures()),
});


export default connect(mapStateToProps, mapDispatchToProps)(Pictures);