import React, {Component, Fragment} from 'react';
import {Alert, Button, Col, Form, FormFeedback, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {registerUser} from "../../store/actions";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

class Register extends Component {
    state = {
        username: '',
        password: ''
    };


    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.registerUser({...this.state})
    };

    getFieldError = fieldName => {
        console.log(this.props);
        return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message;
    };

    render() {
        return (
            <Fragment>
                {this.props.error && this.props.error.global && (
                    <Alert color="danger">
                        {this.props.error.global}
                    </Alert>
                )}
                <Form onSubmit={this.submitFormHandler}>
                    <h2>Register</h2>
                    <FormGroup row>
                        <Label  sm={2}>Username</Label>
                        <Col sm={10}>
                            <Input invalid={!!this.getFieldError('username')} type="text" name="username" value={this.state.username} onChange={this.inputChangeHandler}  placeholder="with a placeholder" />
                                <FormFeedback>
                                    {this.getFieldError('username')}
                                </FormFeedback>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="examplePassword" sm={2}>Password</Label>
                        <Col sm={10}>
                            <Input invalid={!!this.getFieldError('password')} type="password" name="password" value={this.state.password} onChange={this.inputChangeHandler}  placeholder="password placeholder" />
                            <FormFeedback>
                                {this.getFieldError('password')}
                            </FormFeedback>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary">
                                Register
                            </Button>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                           <FacebookLogin/>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>

        );
    }
}

const mapStateToProps = state => ({
    error: state.reducer.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);