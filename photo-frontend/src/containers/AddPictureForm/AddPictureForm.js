import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {createPictures} from "../../store/actions";

class AddPictureForm extends Component {
    state = {
        title: '',
        image: ''
    };

    inputChangeHandler = value => {
        this.setState({
            title: value
        })
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };


    submitFormData = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.createPictures(formData)
    };

    render() {
        return (
           <Fragment>
               <Form onSubmit={this.submitFormData}>
                   <h2>Add new picture</h2>
                   <FormGroup row>
                       <Label  sm={2}>Title</Label>
                       <Col sm={10}>
                           <Input  type="text" name="title"  onChange={(e) => this.inputChangeHandler(e.target.value)}  placeholder="with a placeholder" />
                       </Col>
                   </FormGroup>
                   <FormGroup row>
                       <Label for="examplePassword" sm={2}>Image</Label>
                       <Col sm={10}>
                           <Input type="file" name="image"  onChange={this.fileChangeHandler}/>
                       </Col>
                   </FormGroup>
                   <FormGroup row>
                       <Col sm={{offset: 2, size: 10}}>
                           <Button type="submit" color="primary">
                               Create
                           </Button>
                       </Col>
                   </FormGroup>
               </Form>
           </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createPictures: data => dispatch(createPictures(data))
});

export default connect(null, mapDispatchToProps)(AddPictureForm);