import React, {Component} from 'react';
import {Alert, Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {loginUser} from "../../store/actions";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

class Login extends Component {
    state = {
        username: '',
        password: ''
    };


    inputChangeHandler = event => {
      this.setState({
          [event.target.name]: event.target.value
      })
    };

    submitFormHandler = event => {
      event.preventDefault();

        this.props.loginUser({...this.state})
    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <h2>Login</h2>
                {this.props.error && (
                    <Alert color="danger">
                        {this.props.error.error || this.props.error.global}
                    </Alert>
                )}
                <FormGroup row>
                    <Label  sm={2}>Username</Label>
                    <Col sm={10}>
                        <Input  type="text" name="username" value={this.state.username} onChange={this.inputChangeHandler}  placeholder="with a placeholder" />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="examplePassword" sm={2}>Password</Label>
                    <Col sm={10}>
                        <Input type="password" name="password" value={this.state.password} onChange={this.inputChangeHandler}  placeholder="password placeholder" />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <Button type="submit" color="primary">
                            Login
                        </Button>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <FacebookLogin/>
                    </Col>
                </FormGroup>
            </Form>

         );
    }
}

const mapStateToProps = state => ({
    error: state.reducer.loginError
});

const mapDispatchToProps = dispatch => ({
    loginUser: userData => dispatch(loginUser(userData))
});


export default connect(mapStateToProps, mapDispatchToProps)(Login);