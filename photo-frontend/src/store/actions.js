import axios from '../axios-api';
import {push} from "connected-react-router";
import {NotificationManager} from 'react-notifications'



export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';


export const FETCH_PICTURES_SUCCESS = 'FETCH_PICTURES_SUCCESS';
export const FETCH_PICTURE_ID_SUCCESS = 'FETCH_PICTURE_ID_SUCCESS';

export const registerUserSuccess = (user) => ({type: REGISTER_USER_SUCCESS, user});
export const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, error});

export const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
export const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

export const fetchPicturesSuccess = (pictures) => ({type: FETCH_PICTURES_SUCCESS, pictures});



export const registerUser = userData => {
    return dispatch => {
        return axios.post('/users', userData).then(
            response => {
                dispatch(registerUserSuccess(response.data.user));
                NotificationManager.success('Register successfully!');
                dispatch(push('/'));
            },
            error => {
                console.log(error.response.data);
                if (error.response && error.response.data) {
                    dispatch(registerUserFailure(error.response.data));
                } else {
                    dispatch(registerUserFailure({global: 'No connection'}));
                }
            }
        );
    };
};

export const loginUser = userData => {
    return dispatch => {
        return axios.post('/users/sessions', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user));
                NotificationManager.success('Login in successfully!');
                dispatch(push('/'));
            },
            error => {
                if (error.response && error.response.data) {
                    dispatch(loginUserFailure(error.response.data));
                } else {
                    dispatch(loginUserFailure({global: 'No connection'}));
                }
            }
        )
    }
};

export const facebookLogin = userData => {
    return dispatch => {
        return axios.post('/users/facebookLogin', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user));
                NotificationManager.success('Logged in via Facebook');
                dispatch(push('/'))
            },
            () => {
                dispatch(loginUserFailure('Login via Facebook failed'));
            }
        )
    }
};


export const fetchPictures = (userId) => {
    let url = '/pictures';

    if (userId) {
        url += `?user=${userId}`
    }

    return dispatch => {
        axios.get(url).then(
            response => {
                dispatch(fetchPicturesSuccess(response.data))
            }
        )
    }
};




export const createPictures = data => {
   return dispatch => {
       axios.post('/pictures', data).then(
           () => {
               dispatch(fetchPictures())
               dispatch(push('/'))
           }
       )
   }
};



export const deletePicture = (pictureId, userId) => {
    return dispatch => {
        axios.delete('/pictures/' + pictureId).then(
            () => {
                dispatch(fetchPictures(userId))
            }
        )
    }
};