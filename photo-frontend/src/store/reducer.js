import {
    FETCH_PICTURES_SUCCESS,
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS,
    REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS
} from "./actions";

const initialState = {
    registerError: null,
    loginError: null,
    user: null,
    pictures: [],
};


const reducer = (state = initialState, action) => {
    switch(action.type) {
        case REGISTER_USER_SUCCESS:
            return {...state, registerError: null, user: action.user};
        case REGISTER_USER_FAILURE:
            return {...state, registerError: action.error};
        case LOGIN_USER_SUCCESS:
            return {...state, user: action.user, loginError: null};
        case LOGIN_USER_FAILURE:
            return {...state, loginError: action.error};
        case FETCH_PICTURES_SUCCESS:
            return {...state, pictures: action.pictures};
        default:
            return state;
    }};

export default reducer