import React from 'react';

import './Backdrop.css';

const Backdrop = props => (

    props.isOpen ? <div onClick={props.close} className="Backdrop"></div> : null
);

export default Backdrop;