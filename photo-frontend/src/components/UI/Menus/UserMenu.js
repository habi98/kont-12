import React, {Fragment} from 'react';
import {Button, NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const UserMenu = ({user, logout}) => {
    return (
        <Fragment>
            <NavItem>
                <NavLink tag={RouterNavLink} to={"/pictures/" + user._id}>Hello! {user.username}</NavLink>
            </NavItem>
            <Button color="primary">Logout</Button>
        </Fragment>
    );
};

export default UserMenu;