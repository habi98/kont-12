import React, {Fragment} from 'react';
import './Modal.css'

import Backdrop from '../Backdrop/Backdrop'
import {Button} from "reactstrap";
import CardFooter from "reactstrap/es/CardFooter";

const Modal = props => {
    return (
        <Fragment>
            <Backdrop isOpen={props.isOpen} close={props.close}/>
            <div
                className="Modal"
                style={
                    {transform: props.isOpen ? 'translateY(0)':  'translateY(-100vh)',
                        opacity: props.isOpen ? '1' : 0
                    }
                }>
                {props.children}
                <CardFooter>
                    <Button onClick={props.close}>Close</Button>
                </CardFooter>
            </div>
        </Fragment>
    );
};

export default Modal;