import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import {Provider} from 'react-redux';

import thunkMiddleware from "redux-thunk";
import {createBrowserHistory} from "history";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {ConnectedRouter, connectRouter, routerMiddleware} from "connected-react-router";
import axios from './axios-api';
import reducer from './store/reducer'
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-notifications/lib/notifications.css';


const history = createBrowserHistory();

const rootReducer = combineReducers({
    router: connectRouter(history),
    reducer: reducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


const enhancers = composeEnhancers(applyMiddleware(thunkMiddleware, routerMiddleware(history)));

const store = createStore(rootReducer, enhancers);

axios.interceptors.request.use(config => {

    try {
        config.headers['Authorization'] = store.getState().reducer.user.token
    } catch (e) {

    }

    return config
});




const app = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App/>
        </ConnectedRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
